package io.appmine.hbase.batchloader;

import io.appmine.hbase.test.HBaseEmbedded;
import io.appmine.util.EntityHolder;
import io.appmine.util.ValueHolder;
import junit.framework.TestCase;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.CreateFlag;
import org.apache.hadoop.fs.FileContext;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.client.Get;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.compress.CompressionCodec;
import org.apache.thrift.TSerializer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;

public class HBaseKVMapperDemo {
    private static Configuration configuration;
    private static TSerializer serializer;

    @BeforeClass
    public static void beforeClass() throws IOException {
        // -- start the cluster
        configuration = HBaseEmbedded.startCluster();

        // -- create the test table
        HBaseEmbedded.createTable("test");

        // -- remove the output directory if it exists
        File file = new File("target/small-view.hbase");
        if (file.exists()) file.delete();

        // -- create the thrift serializer
        serializer = new TSerializer();
    }

    @AfterClass
    public static void afterClass() {
        // -- stop the cluster
        HBaseEmbedded.stopCluster();
    }

    @Test
    public void test() throws Exception {
        Driver.main(new String[] { "-i", "src/test/resources/small.seq", "-o", "target/small-view.hbase", "-t", "test"});

        HTable table = new HTable("test");

        Get get = new Get(Bytes.toBytes("key-1"));
        Result result = table.get(get);
        assertFalse(result.isEmpty());

        // -- check if the columns are available
        assertTrue(result.containsColumn(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myBoolProperty")));
        assertTrue(result.containsColumn(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myByteProperty")));
        assertTrue(result.containsColumn(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myDoubleProperty")));
        assertTrue(result.containsColumn(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myIntProperty")));
        assertTrue(result.containsColumn(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myLongProperty")));
        assertTrue(result.containsColumn(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myShortProperty")));
        assertTrue(result.containsColumn(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myStringProperty")));

        // -- check if the columns have the right value
        assertEquals(true, Bytes.toBoolean(result.getColumnLatest(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myBoolProperty")).getValue()));
        assertEquals((byte) 3, result.getColumnLatest(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myByteProperty")).getValue()[0]);
        assertEquals(1.34545345436D, Bytes.toDouble(result.getColumnLatest(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myDoubleProperty")).getValue()));
        assertEquals(15, Bytes.toInt(result.getColumnLatest(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myIntProperty")).getValue()));
        assertEquals(1L, Bytes.toLong(result.getColumnLatest(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myLongProperty")).getValue()));
        assertEquals((short) 128, Bytes.toShort(result.getColumnLatest(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myShortProperty")).getValue()));
        assertEquals("testValue", Bytes.toString(result.getColumnLatest(HBaseKVMapper.DEFAULT_FAMILY, Bytes.toBytes("myStringProperty")).getValue()));
    }
}
