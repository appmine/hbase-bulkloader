package io.appmine.hbase.test;

import io.appmine.util.EntityHolder;
import io.appmine.util.ValueHolder;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.CreateFlag;
import org.apache.hadoop.fs.FileContext;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.thrift.TSerializer;

import java.util.EnumSet;
import java.util.HashMap;

public class GenerateSequenceFile {
    private static TSerializer serializer = new TSerializer();

    public static void main(String[] args) throws Exception {
        Configuration configuration = new Configuration();
        configuration.set(FileSystem.DEFAULT_FS, "file:///");

        SequenceFile.Writer writer = null;
        LongWritable key = new LongWritable();
        BytesWritable value = new BytesWritable();
        FileSystem fs = FileSystem.get(configuration);

        try {
            FileContext fileContext = FileContext.getFileContext(configuration);
            writer = SequenceFile.createWriter(
                    fileContext,
                    configuration,
                    new Path("src/test/resources/small.seq"),
                    key.getClass(),
                    value.getClass(),
                    SequenceFile.CompressionType.NONE,
                    null, new SequenceFile.Metadata(), EnumSet.of(CreateFlag.OVERWRITE, CreateFlag.CREATE)
            );

            // -- set the key
            key.set(1L);

            // -- create the value
            EntityHolder entity = new EntityHolder("key-1", new HashMap<String, ValueHolder>());
            entity.getProperties().put("myBoolProperty", new ValueHolder(ValueHolder._Fields.BOOL_VALUE, true));
            entity.getProperties().put("myByteProperty", new ValueHolder(ValueHolder._Fields.BYTE_VALUE, (byte) 3));
            entity.getProperties().put("myDoubleProperty", new ValueHolder(ValueHolder._Fields.DOUBLE_VALUE, 1.34545345436D));
            entity.getProperties().put("myIntProperty", new ValueHolder(ValueHolder._Fields.INT_VALUE, 15));
            entity.getProperties().put("myLongProperty", new ValueHolder(ValueHolder._Fields.LONG_VALUE, 1L));
            entity.getProperties().put("myShortProperty", new ValueHolder(ValueHolder._Fields.SHORT_VALUE, (short) 128));
            entity.getProperties().put("myStringProperty", new ValueHolder(ValueHolder._Fields.STRING_VALUE, "testValue"));
            byte[] valueBytes = serializer.serialize(entity);
            value.set(valueBytes, 0, valueBytes.length);

            writer.append(key, value);

        } finally {
            if (writer != null) try { writer.close(); } catch (Exception e) {}
        }
    }
}
