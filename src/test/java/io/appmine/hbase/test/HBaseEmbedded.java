package io.appmine.hbase.test;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.zookeeper.MiniZooKeeperCluster;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

public class HBaseEmbedded {
    /** The Constant logger. */
    private static final Logger logger = LoggerFactory.getLogger(HBaseEmbedded.class);

    /** The utility. */
    private static HBaseTestingUtility utility;

    private static Configuration configuration;

    private static Boolean isStarted = false;

    /**
     * Starts a new cluster.
     */
    public static Configuration startCluster()
    {
        if (!isStarted)
        {
            File workingDirectory = new File("./");
            Configuration conf = new Configuration();
            System.setProperty("test.build.data", workingDirectory.getAbsolutePath());
            conf.set("test.build.data", new File(workingDirectory, "zookeeper").getAbsolutePath());
            conf.set(FileSystem.DEFAULT_FS, "file:///");
            conf.set("zookeeper.session.timeout", "180000");
            conf.set("hbase.zookeeper.peerport", "2888");
            conf.set("hbase.zookeeper.property.clientPort", "2181");
            try
            {
                conf.set(HConstants.HBASE_DIR, new File(workingDirectory, "hbase").toURI().toURL().toString());
            }
            catch (MalformedURLException e1)
            {
                logger.error(e1.getMessage());
            }

            configuration = HBaseConfiguration.create(conf);
            utility = new HBaseTestingUtility(configuration);
            try
            {
                MiniZooKeeperCluster zkCluster = new MiniZooKeeperCluster(conf);
                zkCluster.setDefaultClientPort(2181);
                zkCluster.setTickTime(18000);
                zkCluster.startup(new File(workingDirectory, "zookeeper"));
                utility.setZkCluster(zkCluster);
                utility.startMiniCluster();
                utility.getHBaseCluster().startMaster();
            }
            catch (Exception e)
            {
                logger.error(e.getMessage());
                throw new RuntimeException(e);
            }
            isStarted = true;
        }

        return configuration;
    }

    public static void createTable(String tableName)
    {
        try
        {
            if (!utility.getHBaseAdmin().tableExists(tableName))
            {
                utility.createTable(tableName.getBytes(), tableName.getBytes());
            }
            else
            {
                logger.info("Table:" + tableName + " already exist:");
            }
        }
        catch (IOException e)
        {
            logger.error(e.getMessage());
        }
    }

    public static void addColumn(String tableName, String columnFamily)
    {
        try
        {
            utility.getHBaseAdmin().disableTable(tableName);
            utility.getHBaseAdmin().addColumn(tableName, new HColumnDescriptor(columnFamily));
            utility.getHBaseAdmin().enableTable(tableName);
        }
        catch (InvalidFamilyOperationException ife)
        {
            logger.info("Column family:" + columnFamily + " already exist!");
        }
        catch (IOException e)
        {
            logger.error(e.getMessage());
        }
    }

    public static void stopCluster() {
        try
        {
            if (utility != null)
            {
                utility.shutdownMiniCluster();
                utility = null;
            }
        }
        catch (Exception e)
        {
            logger.error(e.getMessage());
        }
    }

    public static HBaseAdmin getHBaseAdmin() throws IOException {
        return utility.getHBaseAdmin();
    }
}
