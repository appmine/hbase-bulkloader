package io.appmine.hbase.batchloader;

import io.appmine.util.EntityHolder;
import io.appmine.util.ValueHolder;
import org.apache.hadoop.hbase.HConstants;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.thrift.TDeserializer;
import org.apache.thrift.TException;

import java.io.IOException;
import java.util.Map;

public class HBaseKVMapper extends Mapper<LongWritable, BytesWritable, ImmutableBytesWritable, KeyValue> {

    final static byte[] DEFAULT_FAMILY = "data".getBytes();

    private TDeserializer deserializer;

    private ImmutableBytesWritable hKey = new ImmutableBytesWritable();

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        deserializer = new TDeserializer();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void map(LongWritable key, BytesWritable value, Context context) throws IOException, InterruptedException {
        EntityHolder entity = new EntityHolder();

        try {
            // -- deserialize the entity holder
            deserializer.deserialize(entity, value.getBytes());

            // -- set the key
            hKey.set(entity.getKey().getBytes());

            // -- create holders for the columnFamily and property name
            long timestamp = HConstants.LATEST_TIMESTAMP;
            byte[] family = DEFAULT_FAMILY;
            byte[] column;

            // -- iterate the properties
            for (Map.Entry<String, ValueHolder> property : entity.getProperties().entrySet()) {

                // -- determine the family and column. the property key can only contain the column name or the family
                // -- and name, separated by a semicolon
                String[] propertyKeyParts = property.getKey().split(":");
                if (propertyKeyParts.length == 1) {
                    column = propertyKeyParts[0].getBytes();
                } else if (propertyKeyParts.length == 2) {
                    family = propertyKeyParts[0].getBytes();
                    column = propertyKeyParts[1].getBytes();
                } else if (propertyKeyParts.length == 3) {
                    family = propertyKeyParts[0].getBytes();
                    column = propertyKeyParts[1].getBytes();
                    timestamp = Long.parseLong(propertyKeyParts[2]);
                } else {
                    context.getCounter("HBaseKVMapper", "INVALID_PROPERTY_KEY_FORMAT").increment(1);
                    continue;
                }

                // -- get the value
                byte[] val = getValue(property.getValue());

                // -- continue if no value was set
                if (val.length == 0) continue;

                // -- write a keyValue to the context
                context.write(hKey, new KeyValue(hKey.get(), family, column, timestamp, val));
            }

        } catch (TException e) {
            context.getCounter("HBaseKVMapper", "DESERIALIZE_FAILED").increment(1);
        }

        context.getCounter("HBaseKVMapper", "PROCESSED_RECORD_COUNT").increment(1);
    }

    protected byte[] getValue(ValueHolder valueHolder) {
        if (valueHolder.isSetBoolValue()) return Bytes.toBytes(valueHolder.getBoolValue());
        else if (valueHolder.isSetByteValue()) return new byte[] { valueHolder.getByteValue() };
        else if (valueHolder.isSetDoubleValue()) return Bytes.toBytes(valueHolder.getDoubleValue());
        else if (valueHolder.isSetIntValue()) return Bytes.toBytes(valueHolder.getIntValue());
        else if (valueHolder.isSetLongValue()) return Bytes.toBytes(valueHolder.getLongValue());
        else if (valueHolder.isSetShortValue()) return Bytes.toBytes(valueHolder.getShortValue());
        else if (valueHolder.isSetStringValue()) return Bytes.toBytes(valueHolder.getStringValue());
        else return new byte[0];
    }
}