package io.appmine.hbase.batchloader;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.PosixParser;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.KeyValue;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat;
import org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.partition.TotalOrderPartitioner;


/**
 * Warning!
 *
 * When I hit this problem, I concluded that HFileOutputFormat cannot be used
 * in standalone mode since it requires DistributedCache, which doesn't work
 * with the local job runner.
 *
 * ^^ taken from http://apache-hbase.679495.n3.nabble.com/local-bulk-loading-td3942691.html
 */
public class Driver {
    private String inputFile;
    private String outputFile;
    private String tableName;

    public static void main(String[] args) throws Exception {
        Driver driver = new Driver();

        try {
            driver.initialize(args);
            driver.launch();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void initialize(String[] args) throws Exception {
        Options options = new Options();
        options.addOption("i", true, "The input path");
        options.addOption("o", true, "The output path");
        options.addOption("t", true, "The table name");

        CommandLine cli = new PosixParser().parse(options, args);

        if (! cli.hasOption("i")) throw new Exception("No input path has been provided");
        if (! cli.hasOption("o")) throw new Exception("No output path has been provided");
        if (! cli.hasOption("t")) throw new Exception("No table name has been provided");

        inputFile = cli.getOptionValue("i");
        outputFile = cli.getOptionValue("o");
        tableName = cli.getOptionValue("t");
    }

    protected void launch() throws Exception {
        Configuration conf = new Configuration();

        conf.set("hbase.table.name", tableName);
        conf.set("mapreduce.jobtracker.address", "localhost:8021");
        conf.set("mapreduce.framework.name", "classic");
        conf.set(TotalOrderPartitioner.PARTITIONER_PATH, "/tmp/partitions_" + System.currentTimeMillis());

        // Load hbase-site.xml
        HBaseConfiguration.addHbaseResources(conf);

        Job job = new Job(conf, "HBase Bulk Import Example");
        job.setJarByClass(HBaseKVMapper.class);

        job.setMapperClass(HBaseKVMapper.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapOutputValueClass(KeyValue.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);

        HTable hTable = new HTable(tableName);

        // Auto configure partitioner and reducer
        HFileOutputFormat.configureIncrementalLoad(job, hTable);

        FileInputFormat.addInputPath(job, new Path(inputFile));
        FileOutputFormat.setOutputPath(job, new Path(outputFile));

        job.waitForCompletion(true);

        // Load generated HFiles into table
        LoadIncrementalHFiles loader = new LoadIncrementalHFiles(conf);
        loader.doBulkLoad(new Path(outputFile), hTable);
    }
}
