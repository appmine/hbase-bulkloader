namespace java io.appmine.util

union ValueHolder {
    1: bool boolValue;
    2: byte byteValue;
    3: i16 shortValue;
    4: i32 intValue;
    5: i64 longValue;
    6: double doubleValue;
    7: string stringValue;
}

struct EntityHolder {
    1: required string key;
    2: required map<string, ValueHolder> properties;
}

